package com.example.bigdataexportexcel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BigdataexportexcelApplication {

    public static void main(String[] args) {
        SpringApplication.run(BigdataexportexcelApplication.class, args);
    }

}
